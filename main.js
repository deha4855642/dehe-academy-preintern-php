// Modal Image Gallery
function onClick(element) {
    document.getElementById("img01").src = element.src;
    document.getElementById("modal01").style.display = "block";
    var captionText = document.getElementById("caption");
    captionText.innerHTML = element.alt;
  }
  
  
  // Toggle between showing and hiding the sidebar when clicking the menu icon
  var mySidebar = document.getElementById("mySidebar");
  
  function w3_open() {
    if (mySidebar.style.display === 'block') {
      mySidebar.style.display = 'none';
    } else {
      mySidebar.style.display = 'block';
    }   
  }
  
  // Close the sidebar with the close button
  function w3_close() {
      mySidebar.style.display = "";
  }

  function signAcc(){
    // Redirect to the signupBasic.html page in the same directory
    window.location.href ='account.php';
  }

  function redirectToSignupPage1() {
    // Redirect to the signupBasic.html page in the same directory
    window.location.href = 'signupBasic.php';
}
function redirectToSignupPage2() {
  // Redirect to the signupBasic.html page in the same directory
  window.location.href = 'signupPro.php';
}
function redirectToSignupPage3() {
  // Redirect to the signupBasic.html page in the same directory
  window.location.href = 'signupPre.php';
}
// Validate the contact form
function validateForm() {
  var name = document.forms["contactForm"]["Name"].value;
  var email = document.forms["contactForm"]["Email"].value;
  var subject = document.forms["contactForm"]["Subject"].value;
  var message = document.forms["contactForm"]["Message"].value;

  if (name === "" || email === "" || subject === "" || message === "") {
    alert("All fields must be filled out");
    return false;
  }

  if (!validateEmail(email)) {
    alert("Invalid email address");
    return false;
  }
  return true;
}

// Validate email format
function validateEmail(email) {
  var emailPattern = /\S+@\S+\.\S+/;
  return emailPattern.test(email);
}


function successSendMessage(){
  if (validateForm()) {
  alert("Message sent successfully!");
  window.location.href = 'index.php';
  }
}

function suggestSearch(event) {
  event.preventDefault(); // Ngăn chặn biểu mẫu gửi đi
  var index = document.getElementById("searchInput").value; // Lấy nội dung tìm kiếm và chuyển

  var searchInput = document.getElementById("searchInput").value.toLowerCase(); // Lấy nội dung tìm kiếm và chuyển thành chữ thường

  var searchResult = document.getElementById("search-result");
  searchResult.innerHTML = ""; // Xóa kết quả trước đó

  if (searchInput) {
      // Tìm kiếm trong danh sách index
      var results = index.filter(function (item) {
          return item.includes(searchInput);
      });

      // Hiển thị kết quả
      if (results.length > 0) {
          searchResult.innerHTML = "Kết quả tìm kiếm:<br>";
          results.forEach(function (result) {
              searchResult.innerHTML += result + "<br>";
          });
      } else {
          searchResult.innerHTML = "Không tìm thấy kết quả nào.";
      }
  }
}

function searchAndHighlight(event) {
  event.preventDefault(); 

  var searchTerm = document.getElementById("searchInput").value.toLowerCase();
  var contentToSearch = document.body.innerHTML;
  var searchRegex = new RegExp(searchTerm, "");
  var highlightedText = contentToSearch.replace(searchRegex, '<span class="highlighted">' + searchTerm + '</span>');

  if (highlightedText !== contentToSearch) {
    document.body.innerHTML = highlightedText;
  } else {
    alert("Không tìm thấy kết quả.");
  }
}
