<?php
session_start();
require_once 'connectDB.php';

// Function to validate a Gmail email address pattern
function isValidGmailAddress($email) {
    return preg_match('/^[A-Za-z0-9._%+-]+@gmail\.com$/', $email);
}

if (isset($_POST['login'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Check if the input is a valid Gmail email address
    if (isValidGmailAddress($username)) {
        // It's a valid Gmail email address
        $query = mysqli_prepare($conn, "SELECT * FROM `user` WHERE `username`=?");
    } else {
        // It's not a valid Gmail email address, treat it as a regular username
        $query = mysqli_prepare($conn, "SELECT * FROM `user` WHERE `username`=?");
    }

    mysqli_stmt_bind_param($query, "s", $username);
    mysqli_stmt_execute($query);
    $result = mysqli_stmt_get_result($query);

    if ($row = mysqli_fetch_assoc($result)) {
        // Verify the entered password with the stored hashed password
        if (password_verify($password, $row['password'])) {
            $_SESSION['user_id'] = $row['user_id'];
            echo "Login successful!"; // Add this for debugging
            header('location: account.php');
        } else {
            echo "<div class='alert alert-danger'>Invalid username or password</div>";
        }
    } else {
        echo "<div class='alert alert-danger'>User not found</div>";
    }

    // Close the prepared statement
    mysqli_stmt_close($query);
}
?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1"/>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="css/login.css">
	</head>


<body>
<div class="col-md-12"></div>
<div class="col-md-12 well"></div>
	<div class="col-md-9 well " style="margin-left: 15%;">
		<h3 class="text-primary glow-on-hover" style="text-align: center;">Login And Register</h3>
		<hr style="border-top:1px dotted #ccc;"/>
		<div class="col-md-6">
			<div class="alert alert-info">No account yet? Register here...</div>
			<form action="" method="POST">
				<div class="form-group">
					<label>Name</label>
					<input type="text" name="name" class="form-control" required="required"/>
				</div>
				<div class="form-group">
					<label>Username</label>
					<input type="text" name="username" class="form-control" required="required"/>
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" maxlength="12" name="password" class="form-control" required="required"/>
				</div>
		
			  <button name="register" class="btn btn-primary">Register</button>
				
			</form>
			<br />
			<?php include'register.php'?>
		</div>

    <div class="col-md-6" style="padding:0 ;vertical-align: center;align-items: center; display:block;border:2px solid black; margin-left:200px;width:300px;height:390px;">
      <div>     
        <h2 style="text-align: center;">Login</h2>
            <div class="imgcontainer">
            <img src="image/arisu-aris.gif" alt="Avatar" class="avatar">
            </div>
        <button onclick="document.getElementById('id01').style.display='block'" style="width:auto; margin:105px 100px 0 110px;" class = "neon-on-hover">Login</button>
        </div>

        <div id="id01" class="modal">
          
        <form class="modal-content animate" action="login.php" method="POST">

            <div class="imgcontainer">
              <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
            <img src="https://aniyuki.com/wp-content/uploads/2021/05/aniyuki-anime-dance-gif-40.gif" alt="Avatar" class="avatar" style="height: 400px; width: 400px;">
            </div>
            <div>
            <div class="form-group ">
                <label>Username</label>
                    <input type="text" name="username" class="form-control" required="required"/>
                </div>
                <div class="form-group">
                <label>Password</label>
                    <input type="password" name="password" class="form-control" required="required"/>
                </div>
                    <button name="login" class="btn btn-primary">Login</button>
                <label>
                    <input type="checkbox" checked="checked" name="remember"> Remember me
                </label>
              </div>

            <div class="" style="background-color:#f1f1f1;">
              <span><button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button></span>
              <a href="forgot.php" onclick="forgotAcc()" class="fa btn-success" style="vertical-align: baseline;float: right;margin-top: 20px; padding:0px">Forgot password?</button>
            </div>
            
          </form>
        </div>
    </div>
  </div>
  
<script>
// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
</body>	
</html>