<?php
session_start();
require_once 'connectDB.php';

?>

<!DOCTYPE html>
<html>
<head>
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href='https://www.bacsiwindows.com/2018/02/tao-hieu-ung-chu-neon-tuyet-dep-chi-bang-css.html' rel='canonical'/>
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
body, html {
  height: 100%;
  line-height: 1.8;
}
.neon-on-hover {
  color: #217bf0;
  background: #000000;
  position: relative;
  z-index: 0;
  border-radius: 15px;
  display: flex;
  width: 60%;
  line-height: 1.5cm;
  display:flex;
  align-items:center;
  }
  
  .neon-on-hover:before {
  content: '';
  background: linear-gradient(45deg, #ff0000, #ff7300, #fffb00, #48ff00, #00ffd5, #002bff, #7a00ff, #ff00c8, #ff0000);
  position: absolute;
  top: -2px;
  left:-2px;
  background-size: 400%;
  z-index: -1;
  filter: blur(0px);
  width: calc(100% + 4px);
  height: calc(100% + 4px);
  animation: glowing 20s  infinite;
  opacity: 0;
  transition: opacity .3s ease-in-out;
  border-radius: 10px;
  }
  
  .neon-on-hover:active {
  color: #f5a9a9
  }
  
  .neon-on-hover:active:after {
  background: transparent;
  background-color: #00ffd5;
  }
  
  .neon-on-hover:hover:before {
  opacity: 1;
  }
  
  .neon-on-hover:after {
  z-index: -1;
  content: '';
  position: absolute;
  background: #111;
  background-color: #00ffd5;
  }
  
  @keyframes neon {
  0% { background-position: 0 0; }
  50% { background-position: 400% 0; }
  100% { background-position: 0 0; }
  }
  
  .neon {
  font-size: 30px;
  color: #12584fb6;
  animation: glow 1s ease-in-out infinite alternate;
  }
  
  @-webkit-keyframes neon {
  from {
    text-shadow: 0 0 10px #c51919ea, 0 0 20px #fff, 0 0 30px #e60073, 0 0 40px #2eaf7e, 0 0 50px #e60073, 0 0 60px #e60073, 0 0 70px #e60073;
  }
  
  to {
    text-shadow: 0 0 20px #dbc816, 0 0 30px #279957, 0 0 40px #2b5ea0, 0 0 50px #ff4da6, 0 0 60px #ff4da6, 0 0 70px #ff4da6, 0 0 80px #ff4da6;
  }
  }
  /* Media query for screens smaller than 600px (e.g., mobile) */
  @media (max-width: 600px) {
    .w3-bar-item {
      padding: 12px; /* Decrease padding for better mobile usability */
    }
    .w3-jumbo {
      font-size: 24px; /* Decrease font size for mobile */
    }
    .w3-large {
      font-size: 16px; /* Decrease font size for mobile */
    }
    /* Add more mobile-specific styles as needed */
  }

  /* Media query for screens between 601px and 1024px (e.g., tablets) */
  @media (min-width: 601px) and (max-width: 1024px) {
    .w3-bar-item {
      padding: 14px; /* You can tweak the padding for tablets */
    }
    .w3-jumbo {
      font-size: 36px; /* Adjust font size for tablets */
    }
    .w3-large {
      font-size: 20px; /* Adjust font size for tablets */
    }
    /* Add more tablet-specific styles as needed */
  }
</style>
</head>
<body>
<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-card" id="myNavbar">
    <a href="#home" class="w3-bar-item w3-button w3-wide neon-on-hover" style="font-size:large;font-family:'Times New Roman', Times, serif">LOGO</a>

    <div class="w3-right w3-hide-small">
      <a href="about.php" class="w2-bar-item w3-button">ABOUT</a>
      <a href="#team" class="w2-bar-item w3-button"><i class="fa fa-user"></i> TEAM</a>
      <a href="about.php" class="w2-bar-item w3-button"><i class="fa fa-th"></i> WORK</a>
      <a href="#pricing" class="w2-bar-item w3-button"><i class="fa fa-usd"></i> PRICING</a>
      <a href="#contact" class="w2-bar-item w3-button"><i class="fa fa-envelope"></i> CONTACT</a>
      <a href="account.php" onclick="signAcc()" class="fa fa-user">Account</button>
      <a href="login.php" class="w2-bar-item w3-button"><i class="fa fa-sign-in"></i> SIGN IN and SIGN UP</a>
      <form action="index.php" onsubmit="return searchAndHighlight(event);" class="header__search" method= "post">
            <input id="searchInput" type="text" class="input-search" onkeyup="suggestSearch(event);" placeholder="Nhập từ khóa" name="noidung" autocomplete="off" maxlength="100" style="width:80%;height:auto;">
            <button type="submit" value="search" class="btn btn-outline-success my-3 my-sm-1" style="width:15%;height:auto;" name="btn"><i class="fa fa-search">Tìm kiếm</i></button>
            <div id="search-result"></div>
</form>
    </div>
    <a href="js:void(0)" class="w3-bar-item w3-button w3-right w3-hide-large w3-hide-medium" onclick="w3_open()">
      <i class="fa fa-bars"></i>
    </a>
  </div>
</div>

<!-- Sidebar on small screens when clicking the menu icon -->
<nav class="w3-sidebar w3-bar-block w3-black w3-card w3-animate-left w3-hide-medium w3-hide-large" style="display:none" id="mySidebar">
  <a href="js:void(0)" onclick="w3_close()" class="w3-bar-item w3-button w3-large w3-padding-16">Close &times;</a>
  <a href="#about" onclick="w3_close()" class="w3-bar-item w3-button">ABOUT</a>
  <a href="#team" onclick="w3_close()" class="w3-bar-item w3-button">TEAM</a>
  <a href="#work" onclick="w3_close()" class="w3-bar-item w3-button">WORK</a>
  <a href="#pricing" onclick="w3_close()" class="w3-bar-item w3-button">PRICING</a>
  <a href="#contact" onclick="w3_close()" class="w3-bar-item w3-button ">CONTACT</a>
  <button onclick="w3_signIn()" class="w3-bar-item w3-button">Account</button>
</nav>

<!-- Header with full-height image -->
<header class="bgimg-1 w3-display-container w3-grayscale-min scan" id="home">
  <div class="w3-display-left w3-text-white neon">
    <span class="w3-jumbo w3-hide-small header1 ">Start something that matters</span><br>
    <span class="w3-xxlarge w3-hide-large w3-hide-medium header1" >Start something that matters</span><br>
    <span class="w3-large" style="color: aquamarine;">Stop wasting valuable time with projects that just isn't you.</span>
    <p class="neon-on-hover"><a href="#about" class="w3-button w3-white w3-padding-large w3-large w3-margin-top w3-opacity w3-hover-opacity-off neon-on-hover">Learn more and start today</a></p>
  </div> 
  <div class="w3-display-bottomleft w3-text-grey w3-large" style="padding:24px 48px">
    <i class="fa fa-facebook-official w3-hover-opacity"></i>
    <i class="fa fa-instagram w3-hover-opacity"></i>
    <i class="fa fa-snapchat w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p w3-hover-opacity"></i>
    <i class="fa fa-twitter w3-hover-opacity"></i>
    <i class="fa fa-linkedin w3-hover-opacity"></i>
  </div>
</header>

<!-- About Section -->
<div class="w3-container" style="padding:128px 16px" id="about">
  <h3 class="w3-center post_title entry-title BSW-font-wave-color default_cursor_cs default_cursor_lands neon" style="font-family: 'Times New Roman', Times, serif; font-size: 45px; font-style: bold;">ABOUT THE COMPANY</h3>
  <p class="w3-center w3-large">Key features of our company</p>
  <div class="w3-row-padding w3-center " style="margin-top:64px">
    <div class="w3-quarter">
      <i class="fa fa-desktop w3-margin-bottom w3-jumbo w3-center "></i>
      <p class="w3-large">Responsive</p>
      <p style="font-size: 24px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
    </div>
    <div class="w3-quarter">
      <i class="fa fa-heart w3-margin-bottom w3-jumbo"></i>
      <p class="w3-large">Passion</p>
      <p style="font-size: 24px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
    </div>
    <div class="w3-quarter">
      <i class="fa fa-diamond w3-margin-bottom w3-jumbo"></i>
      <p class="w3-large">Design</p>
      <p style="font-size: 24px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
    </div>
    <div class="w3-quarter">
      <i class="fa fa-cog w3-margin-bottom w3-jumbo"></i>
      <p class="w3-large">Support</p>
      <p style="font-size: 24px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
    </div>
  </div>
</div>

<!-- Promo Section - "We know design" -->
<div class="w3-container w3-light-grey" style="padding:128px 16px">
  <div class="w3-row-padding">
    <div class="w3-col m6">
      <h3 >We know design.</h3>
      <p style="font-size: 24px;font-family:'Times New Roman', Times, serif;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui sapiente voluptate rem! Corporis reiciendis exercitationem laborum voluptas enim at? Voluptas possimus doloribus reiciendis enim, voluptates repellendus eaque expedita libero aut?<br>tempor incididunt ut labore et dolore.</p>
      <p><a href="#work" class="w3-button w3-black"><i class="fa fa-th">&nbsp;</i> View Our Works</a></p>
    </div>
    <div class="w3-col m6">
      <img class="w3-image w3-round-large" src="https://www.w3schools.com/w3images/phone_buildings.jpg" alt="Buildings" width="1500" height="304">
    </div>
  </div>
</div>



<!-- Team Section -->
<div class="w3-container" style="padding:128px 16px" id="team">
  <h3 class="w3-center post_title entry-title BSW-font-wave-color default_cursor_cs default_cursor_lands neon" style="font-family: 'Times New Roman', Times, serif; font-size: 45px; font-style: bold;">THE TEAM</h3>
  <p class="w3-center w3-large">The ones who runs this company</p>
  <div class="w3-row-padding w3-grayscale " style="margin-top:64px">
    <div class="w3-col l3 m6 w3-margin-bottom ">
      <div class="w3-card ">
        <img src="https://www.w3schools.com/w3images/team2.jpg" alt="John" style="width:100%">
        <div class="w3-container fontStyle">
          <h3 class="glow-on-hover" style="text-align: center;">John Doe</h3>
          <p class="w3-opacity" style="text-align: center;">CEO & Founder</p>
          <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
          <div></div>
          <p class="glow-on-hover"style="width: 50%;margin-left:25%;"><button class="w3-button w3-light-grey w3-block" onclick="contactForm()"><i class="fa fa-envelope "></i> Contact</button></p>
        </div>
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-card">
        <img src="https://www.w3schools.com/w3images/team1.jpg" alt="Jane" style="width:100%">
        <div class="w3-container fontStyle">
          <h3 class="glow-on-hover" style="text-align: center;">Anja Doe</h3>
          <p class="w3-opacity" style="text-align: center;">Art Director</p>
          <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
          <p class="glow-on-hover"style="width: 50%;margin-left:25%;"><button class="w3-button w3-light-grey w3-block" onclick="contactForm()"><i class="fa fa-envelope"></i> Contact</button></p>
        </div>
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-card fontStyle">
        <img src="https://www.w3schools.com/w3images/team3.jpg" alt="Mike" style="width:100%">
        <div class="w3-container">
          <h3 class="glow-on-hover" style="text-align: center;">Mike Ross</h3>
          <p class="w3-opacity" style="text-align: center;">Web Designer</p>
          <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
          <p class="glow-on-hover"style="width: 50%;margin-left:25%;"><button class="w3-button w3-light-grey w3-block" onclick="contactForm()"><i class="fa fa-envelope"></i> Contact</button></p>
        </div>
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-card fontStyle">
        <img src="https://www.w3schools.com/w3images/team4.jpg" alt="Dan" style="width:100%">
        <div class="w3-container">
          <h3 class="glow-on-hover" style="text-align: center;">Dan Star</h3>
          <p class="w3-opacity" style="text-align: center;">Designer</p>
          <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
          <p class="glow-on-hover"style="width: 50%;margin-left:25%;"><button class="w3-button w3-light-grey w3-block" onclick="contactForm()"><i class="fa fa-envelope"></i> Contact</button></p>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Promo Section "Statistics" -->
<div class="w3-container w3-row w3-center w3-dark-grey w3-padding-64">
  <div class="w3-quarter">
    <span class="w3-xxlarge">14+</span>
    <br>Partners
  </div>
  <div class="w3-quarter">
    <span class="w3-xxlarge">55+</span>
    <br>Projects Done
  </div>
  <div class="w3-quarter">
    <span class="w3-xxlarge">89+</span>
    <br>Happy Clients
  </div>
  <div class="w3-quarter">
    <span class="w3-xxlarge">150+</span>
    <br>Meetings
  </div>
</div>
<script type="text/javascript" src="javascript">
      $('#myTable'.DataTable());
</script>
<!-- Work Section -->
<div class="w3-container" style="padding:128px 16px" id="work">
  <h3 class="w3-center post_title entry-title BSW-font-wave-color default_cursor_cs default_cursor_lands neon" style="font-family: 'Times New Roman', Times, serif; font-size: 45px; font-style: bold;">OUR WORK</h3>
  <p class="w3-center w3-large">What we've done for people</p>

  <div class="w3-row-padding" style="margin-top:64px">
    <div class="w3-col l3 m6">
      <img src="https://www.w3schools.com/w3images/tech_mic.jpg" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity " alt="A microphone">
    </div>
    <div class="w3-col l3 m6">
      <img src="https://www.w3schools.com/w3images/tech_phone.jpg" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity" alt="A phone">
    </div>
    <div class="w3-col l3 m6">
      <img src="https://www.w3schools.com/w3images/tech_drone.jpg" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity" alt="A drone">
    </div>
    <div class="w3-col l3 m6">
      <img src="https://www.w3schools.com/w3images/tech_sound.jpg" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity" alt="Soundbox">
    </div>
  </div>

  <div class="w3-row-padding w3-section">
    <div class="w3-col l3 m6">
      <img src="https://www.w3schools.com/w3images/tech_tablet.jpg" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity" alt="A tablet">
    </div>
    <div class="w3-col l3 m6">
      <img src="https://www.w3schools.com/w3images/tech_camera.jpg" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity" alt="A camera">
    </div>
    <div class="w3-col l3 m6">
      <img src="https://www.w3schools.com/w3images/tech_typewriter.jpg" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity" alt="A typewriter">
    </div>
    <div class="w3-col l3 m6">
      <img src="https://www.w3schools.com/w3images/tech_tableturner.jpg" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity" alt="A tableturner">
    </div>
  </div>
</div>

<!-- Modal for full size images on click-->
<div id="modal01" class="w3-modal w3-black" onclick="this.style.display='none'">
  <span class="w3-button w3-xxlarge w3-black w3-padding-large w3-display-topright" title="Close Modal Image">&times;</span>
  <div class="w3-modal-content w3-animate-zoom w3-center w3-transparent w3-padding-64">
    <img id="img01" class="w3-image">
    <p id="caption" class="w3-opacity w3-large"></p>
  </div>
</div>

<!-- Skills Section -->
<div class="w3-container w3-light-grey w3-padding-64">
  <div class="w3-row-padding">
    <div class="w3-col m6 fontStyle">
      <h3>Our Skills.</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br>
      tempor incididunt ut labore et dolore.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br>
      tempor incididunt ut labore et dolore.</p>
    </div>
    <div class="w3-col m6 neon">
      <p class="w3-wide" style="background-color:aquamarine;"><i class="fa fa-camera w3-margin-right"></i>Photography</p>
      <div class="w3-green">
        <div class="w3-container w3-dark-grey w3-center neon-on-hover" style="width:90%;">90%</div>
      </div>
      <p class="w3-wide" style="background-color:aquamarine;"><i class="fa fa-desktop w3-margin-right"></i>Web Design</p>
      <div class="w3-blue">
        <div class="w3-container w3-dark-grey w3-center neon-on-hover" style="width:85%">85%</div>
      </div>
      <p class="w3-wide" style="background-color:aquamarine;"><i class="fa fa-photo w3-margin-right"></i>Photoshop</p>
      <div class="w3-red">
        <div class="w3-container w3-dark-grey w3-center neon-on-hover" style="width:75%">75%</div>
      </div>
    </div>
  </div>
</div>


<!-- Pricing Section -->
<div class="w3-container w3-center w3-dark" style="padding:128px 16px;font-size:25px" id="pricing">
  <h3 class="w3-center post_title entry-title BSW-font-wave-color default_cursor_cs default_cursor_lands neon" style="font-family: 'Times New Roman', Times, serif; font-size: 45px; font-style: bold;">PRICING</h3>
  <p class="w3-large" >Choose a pricing plan that fits your needs.</p>
  <div class="w3-row-padding" style="margin-top:64px">
    <div class="w3-third w3-section">
      <ul class="w3-ul w3-white w3-hover-shadow">
        <li class="w3-blue w3-xlarge w3-padding-32">Basic</li>
        <li class="w3-padding-16"><b>10GB</b> Storage</li>
        <li class="w3-padding-16"><b>10</b> Emails</li>
        <li class="w3-padding-16"><b>10</b> Domains</li>
        <li class="w3-padding-16"><b>Endless</b> Support</li>
        <li class="w3-padding-16">
          <h2 class="w3-wide">$ 10</h2>
          <span class="w3-opacity">per month</span>
        </li>
        <li class="w3-light-grey w3-padding-24" style="display:flex;align-items: center;justify-content: center;">
          <button class="w3-button w3-black w3-padding-large neon-on-hover" style="text-align: center;display: grid;" onclick="redirectToSignupPage1()">Sign Up</button> 
        </li>
      </ul>
    </div>
    <div class="w3-third">
      <ul class="w3-ul w3-white w3-hover-shadow" >
        <li class="w3-red w3-xlarge w3-padding-48">Pro</li>
        <li class="w3-padding-16"><b>25GB</b> Storage</li>
        <li class="w3-padding-16"><b>25</b> Emails</li>
        <li class="w3-padding-16"><b>25</b> Domains</li>
        <li class="w3-padding-16"><b>Endless</b> Support</li>
        <li class="w3-padding-16">
          <h2 class="w3-wide">$ 25</h2>
          <span class="w3-opacity">per month</span>
        </li>
        <li class="w3-light-grey w3-padding-24" style="display:flex;align-items: center;justify-content: center;">
          <button class="w3-button w3-black w3-padding-large neon-on-hover"style="text-align: center;display: grid;" onclick="redirectToSignupPage2()">Sign Up</button>
        </li>
      </ul>
    </div>
    <div class="w3-third w3-section">
      <ul class="w3-ul w3-white w3-hover-shadow">
        <li class="w3-yellow w3-xlarge w3-padding-32">Premium</li>
        <li class="w3-padding-16"><b>50GB</b> Storage</li>
        <li class="w3-padding-16"><b>50</b> Emails</li>
        <li class="w3-padding-16"><b>50</b> Domains</li>
        <li class="w3-padding-16"><b>Endless</b> Support</li>
        <li class="w3-padding-16">
          <h2 class="w3-wide">$ 50</h2>
          <span class="w3-opacity">per month</span>
        </li>
        <li class="w3-light-grey w3-padding-24"style="display:flex;align-items: center;justify-content: center;">
          <a href="signupPre.php"></a>
          <button class="w3-button w3-black w3-padding-large neon-on-hover"style="text-align: center;display: grid;" onclick="redirectToSignupPage3()">Sign Up</button>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Contact Section -->
<div class="w3-container w3-light-grey fontStyle" style="padding:128px 16px;" id="contact">
  <h3 class="w3-center post_title entry-title BSW-font-wave-color default_cursor_cs default_cursor_lands neon" style="font-family: 'Times New Roman', Times, serif; font-size: 45px; font-style: bold;">CONTACT</h3>
  <p class="w3-center w3-large">Lets get in touch. Send us a message:</p>
  <div style="margin-top:48px">
    <p><img src="https://cdn-icons-png.flaticon.com/128/6544/6544041.png"> Hanoi, VN</p>
    <p><img src="https://cdn-icons-png.flaticon.com/128/4018/4018697.png"> Phone: +84 335941869</p>
    <p> <img src="https://cdn-icons-png.flaticon.com/128/693/693423.png"> Email: yoshidahidetoshi123@mail.com</p>
    <br>
    <form action="#" target="_blank" name="contactForm" onsubmit="return validateForm()">
      <p><input class="w3-input w3-border" type="text" placeholder="Name" required name="Name"></p>
      <p><input class="w3-input w3-border" type="email" placeholder="Email" required name="Email"></p>
      <p><input class="w3-input w3-border" type="text" placeholder="Subject" required name="Subject"></p>
      <p><input class="w3-input w3-border" type="text" placeholder="Message" required name="Message"></p>
      <p class="neon-on-hover" style="width: 15%;">
        <button class="w3-button w3-black post_title entry-title BSW-font-wave-color default_cursor_cs default_cursor_lands" style="padding: 0;" type="submit" onclick="successSendMessage()">
          <i class="fa fa-paper-plane "></i> SEND MESSAGE
        </button>
      </p>
    </form>
    <!-- Image of location/map -->
    <img src="https://znews-photo.zadn.vn/Uploaded/lerl/2019_02_11/775A0032_copy.jpg" class="w3-image w3-greyscale" style="width:100%;margin-top:48px">
  </div>
</div>

<!-- Footer -->
<footer class="w3-center w3-black w3-padding-64">
  <a href="#home" class="w3-button w3-light-grey"><i class="fa fa-arrow-up w3-margin-right"></i>To the top</a>
  <div class="w3-xlarge w3-section">
    <i class="fa fa-facebook-official w3-hover-opacity"></i>
    <i class="fa fa-instagram w3-hover-opacity"></i>
    <i class="fa fa-snapchat w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p w3-hover-opacity"></i>
    <i class="fa fa-twitter w3-hover-opacity"></i>
    <i class="fa fa-linkedin w3-hover-opacity"></i>
  </div>
</footer>
 

<script src="main.js"></script>
<script src="//cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajaxlibs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.css"></script>
</body>
</html>
