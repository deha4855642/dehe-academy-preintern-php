<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Signup - Pre</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>
<style>
  /* Thêm CSS cho hiệu ứng neon */


h1 {
  color: #fff; /* Màu chữ trắng */
  text-align: center;
  text-transform: uppercase;
  font-size: 2rem;
  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5); /* Đổ bóng văn bản */
}

form {
  max-width: 400px;
  margin: 0 auto;
  padding: 20px;
  background: rgba(255, 255, 255, 0.1); /* Khung với độ trong suốt */
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.3); /* Hiệu ứng bóng đổ */
  border-radius: 10px; /* Bo góc khung */
  backdrop-filter: blur(10px); /* Hiệu ứng mờ nền */
}

/* Hiệu ứng neon cho nút */
button {
  background: #3f51b5; /* Màu nền nút */
  color: #fff; /* Màu chữ trắng */
  padding: 10px 20px;
  border: none;
  border-radius: 5px;
  text-transform: uppercase;
  letter-spacing: 2px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.3); /* Hiệu ứng bóng đổ */
  transition: box-shadow 0.3s, transform 0.3s;
}
/* CSS cho hiệu ứng neon */
h1 {
  color: #fff; /* Màu chữ trắng */
  text-align: center;
  text-transform: uppercase;
  font-size: 2rem;
  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5); /* Đổ bóng văn bản */
  animation: neon 1.5s infinite alternate; /* Sử dụng animation cho hiệu ứng neon */
}

@keyframes neon {
  0% {
    text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);
  }
  100% {
    text-shadow: 0 0 10px #00f, 0 0 20px #00f, 0 0 30px #00f, 0 0 40px #00f, 0 0 50px #00f;
  }
}

/* Hiệu ứng neon cho nút */
button {
  background: #3f51b5; /* Màu nền nút */
  color: #fff; /* Màu chữ trắng */
  padding: 10px 20px;
  border: none;
  border-radius: 5px;
  text-transform: uppercase;
  letter-spacing: 2px;
  animation: neonButton 1.5s infinite alternate; /* Sử dụng animation cho hiệu ứng neon */
}

@keyframes neonButton {
  0% {
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
  }
  100% {
    box-shadow: 0 0 20px rgba(0, 0, 0, 0.6);
  }
}

button:hover {
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.6); /* Hiệu ứng neon khi hover */
  transform: scale(1.05); /* Hiệu ứng phóng to khi hover */
}

</style>
</head>
<body>
    <script src="../signupForm"></script>
    <h1 style="color:black;">Sign Up for Pre</h1>
    <form id="signupForm" onsubmit="return signupPre();" method="POST">
        <label for="name">Name:</label>
        <input type="text" id="name" name="name" required><br><br>

        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required><br><br>

        <label for="password">Password:</label>
        <input type="password" id="password" name="password" required><br><br>

        <label for="confirm_password">Confirm Password:</label>
        <input type="password" id="confirm_password" name="confirm_password" required><br><br>
        <span id="passwordMatchMessage" style="color: red;"></span><br><br>

        <label for="payment_info">Payment Information:</label>
        <input type="text" id="payment_info" name="payment_info" required><br><br>

        <button type="submit">Sign Up</button>
    </form>
    <script>
  // JavaScript để thêm class 'neon' cho tiêu đề và nút
  document.querySelector('h1').classList.add('neon-text');
  document.querySelector('button').classList.add('neon-button');
</script>

</body>
</html>