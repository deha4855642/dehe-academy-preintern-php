<?php
session_start();
require_once 'connectDB.php';

// Check if the user is logged in
if (isset($_SESSION['id'])) {
    // Now you can retrieve the user's information from the session
    // Sanitize the user ID
    $userID = mysqli_real_escape_string($conn, $_SESSION['id']);

    // Query the database to get the user's information using a prepared statement
    $query = mysqli_prepare($conn, "SELECT * FROM `user` WHERE `id`=?");
    mysqli_stmt_bind_param($query, "s", $userID);  // Assuming user_id is an integer; adjust if necessary

    if (mysqli_stmt_execute($query)) {
        $result = mysqli_stmt_get_result($query);
        $userData = mysqli_fetch_assoc($result);

        if (!$userData) {
            // User not found in the database
            echo "User not found in the database.";
        }
    } else {
        // Error executing the query
        echo "Error: " . mysqli_error($conn);
    }

    // Close the prepared statement
    mysqli_stmt_close($query);
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<style>
 .div-cha {
  position: relative;
  margin: auto;
}
.div-con {
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <a class="navbar-brand neon fa fa-home" href="index.php">Trang Page</a>
        </div>
    </nav>
    <div class="col-md-3"></div>
    <div class="col-md-6 well" style="color: black">
        <h3 class="text-primary">User Account</h3>
        <hr style="border-top: 1px dotted #ccc;"/>
        <div class="col-md-5">
            <?php if (isset($userData['name'])) : ?>
                <h4>Welcome, <?php echo $userData['name']; ?></h4>
            <?php endif; ?>

            <?php if (isset($userData['username'])) : ?>
                <p>Username: <?php echo $userData['username']; ?></p>
            <?php endif; ?>
            <!-- Display other user information here -->
        </div>

        <div>        
            <div class="form-group">
                <input type="button" maxlength="12" value="Logout" name="logout" class="btn-block" required="required" onclick="location.href='logout.php';"/>
            </div>
        <div>
            <img src="image/blue-archive-arisu.gif" style="height: auto; vertical-align: middle; margin: 10px 10px 10px 220px;"/>
         </div>
        </div>
    </div>
</body>
</html>
