function signupPre() {
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var confirm_password = document.getElementById("confirm_password").value;
    var payment_info = document.getElementById("payment_info").value;

    // Perform form validation here
    if (name === '' || email === '' || password === '' || confirm_password === '' || payment_info === '') {
        alert("All fields must be filled out.");
        return false;
    }

    if (password !== confirm_password) {
        passwordMatchMessage.textContent = "Password and Confirm Password do not match.";
        return false;
    } else {
        passwordMatchMessage.textContent = "";
    }
    alert("Đăng ký thành công!");
    window.location.href = 'index.php'; // Chuyển hướng về trang chủ
    return true;
}

function signupPro() {
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var confirm_password = document.getElementById("confirm_password").value;
    var payment_info = document.getElementById("payment_info").value;

    // Perform form validation here
    if (name === '' || email === '' || password === '' || confirm_password === '' || payment_info === '') {
        alert("All fields must be filled out.");
        return false;
    }

    if (password !== confirm_password) {
        passwordMatchMessage.textContent = "Password and Confirm Password do not match.";
        return false;
    } else {
        passwordMatchMessage.textContent = "";
    }

    // If validation is successful, you can submit the form
    alert("Đăng ký thành công!");
    window.location.href = 'index.php'; // Chuyển hướng về trang chủ
    return true;
}

function signupBasic() {
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var confirm_password = document.getElementById("confirm_password").value;
    var payment_info = document.getElementById("payment_info").value;

    // Perform form validation here
    if (name === '' || email === '' || password === '' || confirm_password === '' || payment_info === '') {
        alert("All fields must be filled out.");
        return false;
    }

    if (password !== confirm_password) {
        passwordMatchMessage.textContent = "Password and Confirm Password do not match.";
        return false;
    } else {
        passwordMatchMessage.textContent = "";
    }

    alert("Đăng ký thành công!");
    window.location.href = 'index.php'; // Chuyển hướng về trang chủ
    return true;
}

// Thêm hiệu ứng neon bằng JavaScript
const button = document.querySelector('button');
button.addEventListener('mouseenter', () => {
  button.classList.add('neon');
});

button.addEventListener('mouseleave', () => {
  button.classList.remove('neon');
});
