<?php
require_once 'connectDB.php';

// Function to generate hashed password if not already defined
if (!function_exists('generateHashedPassword')) {
    function generateHashedPassword($password) {
        return password_hash($password, PASSWORD_DEFAULT);
    }
}

// Check if the registration form is submitted
if (isset($_POST['register'])) {
    $name = $_POST['name'];
    $username = trim($_POST['username']);
    $password = $_POST['password'];
    $hashedPassword = generateHashedPassword($password);

    // Check database connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    // Prepare the statement to check if the username already exists
    $checkQuery = mysqli_prepare($conn, "SELECT * FROM `user` WHERE `username`=?");

    // Check for errors in query preparation
    if (!$checkQuery) {
        die("Error in query preparation: " . mysqli_error($conn));
    }

    // Bind parameters and execute the check query
    mysqli_stmt_bind_param($checkQuery, "s", $username);
    if (mysqli_stmt_execute($checkQuery)) {
        // Fetch the result
        $result = mysqli_stmt_get_result($checkQuery);

        // Check if the username already exists
        if (mysqli_num_rows($result) > 0) {
            echo "<div class='alert alert-danger'>Username already exists. Choose a different username.</div>";
        } else {
            // Close the check statement
            mysqli_stmt_close($checkQuery);

            // Proceed with registration
            $query = mysqli_prepare($conn, "INSERT INTO `user` (`name`, `username`, `password`) VALUES (?, ?, ?)");
            mysqli_stmt_bind_param($query, "sss", $name, $username, $hashedPassword);

            // Execute the registration query
            if (mysqli_stmt_execute($query)) {
                echo "<div class='alert alert-success'>Successfully registered!</div>";
            } else {
                // Registration failed
                echo "<div class='alert alert-danger'>Registration failed. Please try again.</div>";
            }

            // Close the registration statement
            mysqli_stmt_close($query);
        }
    } else {
        // Error executing the check query
        echo "<div class='alert alert-danger'>Error executing query: " . mysqli_error($conn) . "</div>";
    }

    // Close the connection (if needed)
    // mysqli_close($conn);
}
?>
